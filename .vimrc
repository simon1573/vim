set mouse=a 			" allow mouse usage "
set spelllang=sv,en_us	 	" spellchecking for swedish and english "

"set smartindent" 		" indent code "
syntax on			" activate syntax highlighting "
set number			" show numbered rows "
set t_Co=256

" Needed for Powerline to work "
set nocompatible   " Disable vi-compatibility
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs

autocmd FileType tex setlocal spell spellang=en,sv

" Show nerdtree per default if vim is opened without any args"
"autocmd StdinReadPre * let s:std_in=1 "
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif"

filetype off 				" Requred by Vundle.

set rtp+=~/.vim/bundle/Vundle.vim
" Put plugins here! "
call vundle#begin()

" Initiate Vundle "
Plugin 'gmarik/Vundle.vim'

" Git interface for Vim "
Plugin 'tpope/vim-fugitive'

" Powerline "
Plugin 'bling/vim-airline'

" Nerdtree "
Plugin 'scrooloose/nerdtree'

" Color themes plugin "
Plugin 'flazz/vim-colorschemes'

" You complete me "
" Plugin 'Valloric/YouCompleteMe' 

" Limelight (distraction helper) "
Plugin 'junegunn/limelight.vim'

" VimRoom (distrction helper) "
Plugin 'mikewest/vimroom'

" Indentation lines "
Plugin 'vim-scripts/bib_autocomp.vim'

" CtrlP "
Bundle 'kien/ctrlp.vim'

" Gotham theme "
"Plugin 'whatyouhide/vim-gotham'

" JS lint checker "
Plugin 'Shutnik/jshint2.vim'

" Syntastic! "
Plugin 'scrooloose/syntastic'

" Brackets adder "
Plugin 'Townk/vim-autoclose'

"Latex box"
Plugin 'LaTeX-Box-Team/LaTeX-Box'

" Automatic TeX Plugin "
Plugin 'coot/atp_vim'

" UltiSnips! "
"Plugin 'SirVer/ultisnips'"

" Snippets are separated from the engine. Add this if you want them:
" Plugin 'honza/vim-snippets'

" Neocomplete "
"Plugin 'Shougo/neocomplete.vim'"

"HTML5"
"Plugin 'othree/html5.vim'"

"JS Libs syntax"
Plugin 'othree/javascript-libraries-syntax.vim'

"Angular"
"Plugin 'burnettk/vim-angular'"

"CSS"
Plugin 'lepture/vim-css'

"JS prettyfier"
Plugin 'maksimr/vim-jsbeautify'

" Emmet abbreviations"
"Plugin 'mattn/emmet-vim'"

"JSON formatter"
" Plugin 'XadillaX/json-formatter.vim'

"Bubblegum theme"
" Plugin 'baskerville/bubblegum'

" Hard mode! "
Plugin 'wikitopian/hardmode'

" Relative numbers switch "
Plugin 'jeffkreeftmeijer/vim-numbertoggle'

" GTFO (Terminal and filemanager spawner)"
" Plugin 'justinmk/vim-gtfo'

" Papercolor theme"
Plugin 'NLKNguyen/papercolor-theme'

" Android plugin for Vim "
Plugin 'hsanson/vim-android'

" Word highlighter "
Plugin 'vasconcelloslf/vim-interestingwords'

" ctags generator thingy"
Plugin 'szw/vim-tags'
" Vim polygot, syntax highlightning"
Plugin 'sheerun/vim-polyglot'

call vundle#end()
" Fix for powerline-fonts for Airline "
let g:airline_powerline_fonts = 1

" Start with limelight enabled "
"autocmd VimEnter * Limelight"

filetype plugin indent on 		" Req. by Vundle.
set foldmethod=manual

" Setup indentation acording to SO "
set tabstop=4
set shiftwidth=4
set expandtab

" Better copy paste "
:set clipboard^=unnamed "adds unnamed to existing values

" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Set paths for vim-android "
let g:android_sdk_path = "/home/simon/.android-SDK"
let g:gradle_path = "/usr/bin/gradle"

" More natrual split navigation "
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <Leader>vr :VimroomToggle<CR>

" Syntax check for php "
map <C-B> :!php -l %<CR>

nmap <C-M> :!python2 %<CR>

" Remap leader key "
:let mapleader = "-"

" Remap local leader key"
:let maplocalleader = "\\"

" Loads the color theme "
set background=dark
colorscheme Tomorrow-Night
:let g:airline_theme='PaperColor'
:let g:Powerline_symbols = 'fancy'

